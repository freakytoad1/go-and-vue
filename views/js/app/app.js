const app = Vue.createApp({
    data() {
        return {
            url: "https://plex.freakytoad1.tech/",
            showBooks: true,
            books: [
                { title: 'book title1', author: 'name1', img: "../../assets/1.png", isFav: true},
                { title: 'book title2', author: 'name2', img: "../../assets/2.png", isFav: false},
                { title: 'book title3', author: 'name3', img: "../../assets/3.jpg", isFav: true},
            ],
            x: 0,
            y: 0
        }
    },
    methods: {
        toggleBooks () {
            this.showBooks = !this.showBooks
        },
        toggleFav (book) {
            book.isFav = !book.isFav
        },
        handleEvent(e, data) {
            console.log(e, e.type)
            if (data) {
                console.log(data)
            }
        },
        handleMouseMove(e) {
            this.x = e.offsetX
            this.y = e.offsetY
        }
    },
    computed: {
        filteredBooks() {
            return this.books.filter((book) => book.isFav)
        }
    }
})

app.mount("#app")